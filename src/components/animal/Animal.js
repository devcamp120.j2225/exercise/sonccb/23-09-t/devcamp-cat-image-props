import { Component } from "react";
import cat from '../../assets/images/cat.jpg';

class Animal extends Component {
     render() {
          let { kind } = this.props;
          if (kind === 'cat') {
               return (
                    <>
                         
                         <img src={cat} alt='cat img'></img>
                    </>
               )
          } else {
               return (
                    <>
                         <p>{` meow not found :)`}</p>
                    </>
               )
          }
     }
}

export default Animal;
