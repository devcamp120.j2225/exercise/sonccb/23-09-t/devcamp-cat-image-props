import Animal from "./components/animal/Animal";

function App() {
  return (
    <div>
     <Animal kind="cat"></Animal>
    </div>
  );
}

export default App;
